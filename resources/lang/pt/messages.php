<?php

return [
    'welcome' => 'Situação de chamados',
    'opened_tickets' => 'Chamados em aberto',
    'unassigned_tickets' => 'Chamados sem responsável',
    'add_ticket' => 'Chamado',
    'add_staff' => 'Equipe',
    'search_tickets' => 'Buscar chamado',
    'tickets' => 'Chamados',
    'new' => 'Novo',
    'create_new_ticket' => 'Criar novo chamado',
    'edit' => 'Editar',
    'message' => 'Mensagem',

    'ticket_small_subject' => 'Título resumido do chamado',
    'ticket_small_subject_description' => 'Um título resumido sobre o assunto do chamado, até 60 caracteres.',
    'ticket_subject' => 'Título completo do chamado',
    'ticket_subject_description' => 'Um título completo sobre o assunto do chamado.',
    'enter_subject' => 'Digite o título',
    'choose_department' => 'Escolha um departamento',
    'department' => 'Departamento',
    'choose_prior' => 'Escolha uma prioridade',
    'prior' => 'Prioridade',
    'choose_assignment' => 'Escolha o responsável',
    'assigned_to' => 'Responsável',
    'choose_observers' => 'Escolha os observadores',
    'observers' => 'Observadores',
    'content' => 'Conteúdo',
    'reply_ticket' => 'Responder chamado',
    'ticket_history' => 'Histórico do chamado',
    'ticket_messages' => 'Mensagens do chamado',
    'ticket_no_messages' => 'Não há mensagens para esse ticket',

    'accept_date' => 'Data limite para aceitar',
    'date_placeholder' => '__ / __ / ____',
    'estimated_time' => 'Tempo estimado para a tarefa',
    'time_placeholder' => '__ : __',

    'me' => 'Eu',
    'action' => 'Ações'
];