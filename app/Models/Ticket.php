<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{

    public function user() {
        return $this->BelongsTo(User::class, 'user_id', 'id');
    }

    public function agent() {
        return $this->BelongsTo(User::class, 'agent_user_id', 'id');
    }

    public function messages() {
        return $this->hasMany(Message::class);
    }

    public function prior() {
        return $this->BelongsTo(Prior::class);
    }

    public function department() {
        return $this->BelongsTo(Department::class);
    }

    public function  status() {
        return $this->belongsTo(Status::class);
    }

    public function getNumberOfAlertsAttribute() {

        $lastTicketAccess = UserTicketAccess::where('ticket_id', $this->id )
            ->where('user_id', \Auth::user()->id )
            ->orderBy('created_at', 'DESC')
            ->first();

        $total = 0;

        if ( $lastTicketAccess == null ) {
            $total++;
            $lastTicketAccess = '0000-00-00 00:00:00';
        } else {
            $lastTicketAccess = $lastTicketAccess->created_at;
        }

        $messages = Message::where('ticket_id', $this->id )
            ->where('created_at', '>', $lastTicketAccess)
            ->get();

        $total += $messages->count();


        return $total;
    }

}
